#!/usr/bin/env python

import redis
import json
import uuid
import random
from datetime import datetime

aantal_users = 8
aantal_frames = 2

r = redis.Redis(host="192.168.169.2")
excercise = ["ArmsUp", "LegsUp", "HeadUp"]

def createKeypoints(aantal):
    keypoints = {}
    for i in range(1,aantal):
        obj = {
            "position" : {
               "pos_x" : random.randint(0,255),
               "pos_y" : random.randint(0,255)
            }
        }
        keypoints[i] = obj
    return keypoints

def createMeasurement(testId, userId, excerciseId, frame_id):
    obj = {
        "testId": str(testId),
        "userId": str(userId),
        "frame": {
            "id" : frame_id,
            "excercise" : excerciseId,
            "keypoints" : createKeypoints(17),
            "timestamp" : str(datetime.now(tz=None)),
        }
    }
    return obj

def main():
    for i in range(0,aantal_users):
        userId = uuid.uuid4()    
        testId = uuid.uuid4()
        excerciseId = excercise[random.randint(0, len(excercise)-1)],
        for frame_id in range(0,aantal_frames):
            obj = createMeasurement(testId, userId, excerciseId, frame_id)
            r.set(str(testId)+"-"+str(frame_id), json.dumps(obj))
            # print(json.dumps(obj))


if __name__ == '__main__':
    main()